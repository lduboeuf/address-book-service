pkg_get_variable(SYSTEMD_USER_UNIT_DIR systemd systemduserunitdir)

configure_file(address-book-service.service.in ${CMAKE_CURRENT_BINARY_DIR}/address-book-service.service)
configure_file(address-book-updater.service.in ${CMAKE_CURRENT_BINARY_DIR}/address-book-updater.service)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/address-book-service.service
              ${CMAKE_CURRENT_BINARY_DIR}/address-book-updater.service
        DESTINATION ${SYSTEMD_USER_UNIT_DIR})

